'use strict';
const assert = require('chai').assert;
const fs = require('fs');
const request = require('supertest');
const express = require('express');
const IDENTITY_PUBLIC_KEY = fs.readFileSync('test/fixtures/identity.pem', 'utf8').trim();
const expressMiddleware = require('../lib/expressMiddleware');

let server;

before(() => {
  server = createServer({'127.0.0.1:4500': 'tenantTest'}, 'local');
});

describe('Tenant Access Manager middleware', () => {

  describe('General requests namespace test:', () => {
    it('should be able to resolve tenantId', (done) => {
      request(server)
        .get('/test/getTenantId')
        .expect(200)
        .expect((res) => {
          const body = JSON.parse(res.text);
          assert.equal(body.tenantId, 'tenantTest');
        })
        .end(done);
    });

    it('should be able to resolve tenantId with x-tenant in local namespace', (done) => {
      request(server)
        .get('/test/getTenantId')
        .set('x-tenant', 'specificTenant')
        .expect(200)
        .expect((res) => {
          const body = JSON.parse(res.text);
          assert.equal(body.tenantId, 'specificTenant');
        })
        .end(done);
    });
  });

  describe('General requests namespace production:', () => {
    before(() => {
      server.close();
      server = createServer({'127.0.0.1:4500': 'tenantTest'}, 'prod');
    });

    it('should not be able to resolve specific tenantId with header x-tenant in prod namespace', (done) => {
      request(server)
        .get('/test/getTenantId')
        .expect(200)
        .set('x-tenant', 'specificTenant')
        .expect((res) => {
          const body = JSON.parse(res.text);
          assert.notEqual(body.tenantId, 'specificTenant');
          assert.equal(body.tenantId, 'tenantTest');
        })
        .end(done);
    });

  });
});

function createServer(tenantMap, namespace) {
  let parser = expressMiddleware({
    secret: IDENTITY_PUBLIC_KEY,
    service: 'api',
    namespace: namespace,
    tenantMap: tenantMap,
    jwtoptions: {
      expiresIn: 18000,
      algorithms: ['RS256']
    },
    publicActions: {
       'GET/test/getTenantId': true
    }
  });
  const app = express()
    .use((req, res) => {
      parser(req, res, (err) => {
        res.statusCode = err ? (err.status || 500) : 200;
        let out = res.locals;
        res.end(err ? err.message : JSON.stringify(out));
      });
    });
  return app.listen(4500);
}
