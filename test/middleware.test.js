'use strict';
const assert = require('chai').assert
const jwt = require('jsonwebtoken')
const fs = require('fs')
const should = require('should');
const request = require('supertest')
const _ = require('lodash')
const express = require('express')
const identityFixture = JSON.parse(fs.readFileSync('test/fixtures/identity.json', 'utf8'));

const UnauthorizedError = require('cobuild-access-manager').UnauthorizedError
const Identity = require('cobuild-access-manager').Identity
const Policy = require('cobuild-access-manager').Policy
const AccessManager = require('cobuild-access-manager').AccessManager
const expressMiddleware = require('../lib/expressMiddleware')

let server;

before(function() {
   server = createServer(true);
})

describe('Policy Access Manager middleware', function() {  

  it('should throw 403 statusCode', function() {
    let token = jwt.sign(identityFixture, 'secret2')
    request(server)
      .get('/')
      .set('Authorization', 'Bearer '+token)
      .expect(403)
  })
  it('should not add identity to res.locals', function() {
    let token = jwt.sign(identityFixture, 'secret2')
    request(server)
      .get('/')
      .set('Authorization', 'Bearer '+token)
      .expect(403)
      .end(function (err, res) {
        assert.equal(res.locals, null)
    });
  })
  it('should authorize request', function() {
    let token = jwt.sign(identityFixture, 'secret')
    request(server)
      .get('/')
      .set('Authorization', 'Bearer '+token)
      .expect(200)
  })    

  //This unit parcially test out the middleware - the mock express
  // middleware cannot send the class objects with the functions
  it('should add identity and accessManager to res.locals', function(done) {
    let token = jwt.sign(identityFixture, 'secret')
    request(server)
    .get('/artists')
    .set('Authorization', 'Bearer '+token)
    .expect(200)
    .expect(function(res) {
      var locals = JSON.parse(res.text)
        assert.isNotNull(locals, null)
        assert.isNotNull(locals.identity, null)
        assert.isNotNull(locals.accessManager, null)
      assert.equal(locals.identity.uid, identityFixture.uid)
    })
    .end(done);
  })

  it('should throw 403 Forbidden due the action request', function(done) {
    let token = jwt.sign(identityFixture, 'secret')
    request(server)
    .get('/transactions')
    .set('Authorization', 'Bearer '+token)
    .expect(403)
    .expect(function(res) {
      var locals = JSON.parse(res.text)
        assert.isUndefined(locals.identity)
        assert.isNotNull(locals.err)
    })
    .end(done);
  })

  it('should throw 401 statusCode for private route', function(done) {
    request(server)
      .get('/')       
      .expect(401)
      .end(done);
  })
  it('should throw 200 statusCode for public route', function(done) {
    request(server)
      .get('/playlist/getPlaylistSlide')
      .expect(200)
      .expect(function(res) {
        var body = JSON.parse(res.text)
        //console.log("eee ", body)
      })
     .end(done);
  })

  it('Should return 401 - TokenExpiredError if token expired', () => {
    let payload = JSON.parse(JSON.stringify(identityFixture))
    payload.iat = Math.floor(Date.now() / 1000)
    payload.exp = Math.floor(Date.now() / 1000) - 10  
    let token = jwt.sign(payload, 'secret')
    assert.throws(
      () => { Identity.fromJwtToken(token, 'secret') },
      'jwt expired'
    )
  })

  it('Should not return 401 - TokenExpiredError if token still valid', () => {
    let payload = JSON.parse(JSON.stringify(identityFixture))
    payload.iat = Math.floor(Date.now() / 1000) 
    payload.exp = Math.floor(Date.now() / 1000) + 10000
    let token = jwt.sign(payload, 'secret')
    assert.doesNotThrow(
      () => { Identity.fromJwtToken(token, 'secret') },
      jwt.TokenExpiredError
    )
  })

  it('Should allow send Authorization header to public endpoints', (done) => {    
    let token = jwt.sign(identityFixture, 'secret')  
    request(server)
    .get('/playlist/getPlaylistSlide')
    .set('Authorization', 'Bearer '+token)
    .expect(200)
    .expect(function(res) {
      var body = JSON.parse(res.text)
    })
    .end(done);
  })

   it('Should not allow send wrong Authorization header to public endpoints', (done) => {    
    let token = jwt.sign(identityFixture, 'secret-bad')  
    request(server)
    .get('/playlist/getPlaylistSlide')
    .set('Authorization', 'Bearer '+token)
    .expect(401)
    .expect(function(res) {
      var body = JSON.parse(res.text)
    })
   .end(done);
  })

  it('Should not allow send wrong Authorization header to private endpoints', (done) => {    
    let token = jwt.sign(identityFixture, 'secret-bad')  
    request(server)
    .get('/artists')
    .set('Authorization', 'Bearer '+token)
    .expect(401)
    .expect(function(res) {
      var body = JSON.parse(res.text)
    })
   .end(done);
  })

  //This unit parcially test out the middleware - the mock express
  // middleware cannot send the class objects with the functions
  it('should add identity and accessManager to res.locals for public endpoints if valid auth sent', function(done) {
    let token = jwt.sign(identityFixture, 'secret')
    request(server)
    .get('/playlist/getPlaylistSlide')
    .set('Authorization', 'Bearer '+token)
    .expect(200)
    .expect(function(res) {
      var locals = JSON.parse(res.text)
      assert.isNotNull(locals, null)
      assert.isNotNull(locals.identity, null)
      assert.isNotNull(locals.accessManager, null)
      assert.equal(locals.identity.uid, identityFixture.uid)
    })
    .end(done);
  })

})

function createServer(outputLocals) {

    let parser = expressMiddleware({secret : "secret", service: 'api', publicActions: {index:true, "GET/playlist/getPlaylistSlide":true} })

    return express()
    .use(function(req, res) {
      parser(req, res, function(err){
        res.statusCode = err ? (err.status || 500) : 200;
        var out = outputLocals === true ? res.locals : req.body
        res.end(err ? err.message : JSON.stringify(out));
      })
    })
}