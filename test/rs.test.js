'use strict';
const assert = require('chai').assert
const jwt = require('jsonwebtoken')
const fs = require('fs')
const should = require('should');
const request = require('supertest')
const _ = require('lodash')
const express = require('express')
const identityFixture = JSON.parse(fs.readFileSync('test/fixtures/identity.json', 'utf8'));
const serviceFixture = JSON.parse(fs.readFileSync('test/fixtures/service.json', 'utf8'));
const IDENTITY_PUBLIC_KEY = fs.readFileSync('test/fixtures/identity.pem', 'utf8').trim();
const IDENTITY_PRIVATE_KEY = fs.readFileSync('test/fixtures/identity.key', 'utf8').trim();
const API_PRIVATE_KEY = fs.readFileSync('test/fixtures/api.key', 'utf8').trim();
const UnauthorizedError = require('cobuild-access-manager').UnauthorizedError
const Identity = require('cobuild-access-manager').Identity
const Policy = require('cobuild-access-manager').Policy
const AccessManager = require('cobuild-access-manager').AccessManager
const expressMiddleware = require('../lib/expressMiddleware')

let server;

before(function() {
   server = createServer(true);
})

describe('Policy Access Manager middleware', function() { 

  describe('General requests:', function() { 
    
    it('Error: should throw 401 statusCode for private route', function(done) {
      request(server)
      .get('/artists')       
      .expect(401)
      .end(done);
    })
    it('should throw 200 statusCode for public route', function(done) {
      request(server)
      .get('/playlist/getPlaylistSlide')
      .expect(200)
      .expect(function(res) {
        var body = JSON.parse(res.text)
      })
     .end(done);
    })
  })

  describe('Authorization requests:', function() { 
    it('Error: authorization token is not complete', function(done) {
      request(server)
      .get('/')
      .set('Authorization', 'Bearer')
      .expect(401)
      .end(done)
    })
    it('Error: authorization token is not complete but has a space after Bearer keyword', function(done) {
      request(server)
      .get('/')
      .set('Authorization', 'Bearer ')
      .expect(401)
      .end(done)
    })
    it('Error: token with wrong signature using HS256', function(done) {
      let token = jwt.sign(identityFixture, 'secret')
      request(server)
      .get('/')
      .set('Authorization', 'Bearer '+token)
      .expect(401)
      .end(done)
    })
    it('Error: policy does not have access to specific path', function(done) {
      let token = jwt.sign(identityFixture, IDENTITY_PRIVATE_KEY, { algorithm: 'RS256'})
      request(server)
      .get('/users')
      .set('Authorization', 'Bearer '+token)
      .expect(403)
      .end(done)
    })
    it('should add identity and accessManager to res.locals', function(done) {
      let token = jwt.sign(identityFixture, IDENTITY_PRIVATE_KEY, { algorithm: 'RS256'})
      request(server)
      .get('/artists')
      .set('Authorization', 'Bearer '+token)
      .expect(200)
      .expect(function(res) {
        var locals = JSON.parse(res.text)
          assert.isNotNull(locals)
          assert.isNotNull(locals.identity)
          assert.isNotNull(locals.accessManager)
        assert.equal(locals.identity.uid, identityFixture.uid)
      })
      .end(done);
    })
  })
  describe('x-authorization requests:', function() { 
    it('Error: authorization token is not complete', function(done) {
      request(server)
      .get('/')
      .set('x-authorization', 'Bearer')
      .expect(401)
      .end(done)
    })
    it('Error: authorization token is not complete but has a space after Bearer keyword', function(done) {
      request(server)
      .get('/')
      .set('x-authorization', 'Bearer ')
      .expect(401)
      .end(done)
    })
    it('Error: token with wrong signature using HS256', function(done) {
      let token = jwt.sign(identityFixture, 'secret')
      request(server)
      .get('/')
      .set('x-authorization', 'Bearer '+token)
      .expect(401)
      .end(done)
    })
    it('Error: token does not have important options (issuer and audience) ', function(done) {
      let token = jwt.sign(serviceFixture, API_PRIVATE_KEY,  { algorithm: 'RS256'})
      request(server)
      .get('/artists')
      .set('x-authorization', 'Bearer '+token)
      .expect(401)
      .end(done)
    })
    // Please check integration test for success cases. it does not run successfully here because it is reading secrets and here we do not have access to that.
  })
})

function createServer(outputLocals) {

    console.log('createServer', outputLocals)
    let parser = expressMiddleware({
      secret : IDENTITY_PUBLIC_KEY, 
      service: 'api', 
      jwtoptions: {
        expiresIn: 18000,  //5 hour 
        algorithms: ['RS256']
      },
      publicActions: {
        index:true, "GET/playlist/getPlaylistSlide":true
      } 
    })
    return express()
    .use(function(req, res) {
      parser(req, res, function(err){
        res.statusCode = err ? (err.status || 500) : 200;
        var out = outputLocals === true ? res.locals : req.body
        res.end(err ? err.message : JSON.stringify(out));
      })
    })
}