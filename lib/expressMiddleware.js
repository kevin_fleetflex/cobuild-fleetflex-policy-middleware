'use strict'
const log               = require('debug')('cobuild-express-policy-middleware')
const util              = require('util')
const Identity          = require('cobuild-access-manager').Identity
const Service          = require('cobuild-access-manager').Service
const Policy            = require('cobuild-access-manager').Policy
const AccessManager     = require('cobuild-access-manager').AccessManager
const pathutil          = require('cobuild-access-manager').pathutil
const UnauthorizedError = require('cobuild-access-manager').UnauthorizedError
const ForbiddenError    = require('cobuild-access-manager').ForbiddenError
const jwt = require('jsonwebtoken')
const fs = require('fs')

function expressMiddleware(options) {

  if(!options || !options.secret) throw new Error('Identity secret required to init identityParser Middleware')

  var tokenHeadersParser = function(headers){
    let parts = [];
    if(!headers || (!headers.authorization && !headers["x-authorization"]))
      throw new UnauthorizedError('authorization_header_required', { message: 'No authorization header was found' })
    let headerTokenName= headers.authorization? "authorization": "x-authorization"
    parts = headers[headerTokenName].split(' ');
    if(!parts.length > 0 || !/^Bearer$/i.test(parts[0]) || typeof(parts[1]) !== 'string' || parts[1] == '')
      throw new UnauthorizedError('authorization_bad_format', { message: 'Invalid authorization header format - Bearer [token] expected.' })
    else if(headerTokenName == "x-authorization"){
      try{
        let decoded = jwt.decode(parts[1], {complete: true})
        log("token decoded: ", !!decoded)
        options.secret = fs.readFileSync("/run/secrets/"+decoded.payload.iss+"_public_key", 'utf8').trim() 
        log("options.secret: ", !!options.secret)
      }catch(e){
        throw new UnauthorizedError('authorization_bad_format', { message: 'Invalid authorization header format - Bearer [token] expected.' })          
      }
    } 
    return parts[1]
  }

  if (!Reflect.has(options, 'tenantMap') || typeof options.tenantMap !== 'object') {
    options.tenantMap = {};
  }

  var publicActions = {}
  log("typeof options.publicActions ", typeof options.publicActions)
  if(options.publicActions && typeof options.publicActions === "object"){
    publicActions = options.publicActions
    log('Using custom publicActions', publicActions)
  }

  //Validation of action access can be delegated to the app implementing the middleware
  //A function can be passed through options.accessValidation variable to replace default verficiation
  var accessValidation = null
  var default_accessValidation = function(action, accessManager, reqMethod){
    if (!accessManager.serviceActionAllowed() && ( !publicActions[action] && !publicActions.hasOwnProperty(reqMethod)) )   
      throw new ForbiddenError('action_not_allowed', { message: 'No authorization to execute requested action ' + action})
    else
      return true
  }

  if(options.accessValidation && typeof options.accessValidation === "function"){
    accessValidation = options.accessValidation
    log('Using custom accessValidation')
  }else{
    accessValidation = default_accessValidation
    log('Using default accessValidation')
  }  

  return function policyParser(req, res, next) {

    try{

      if (req.method === 'OPTIONS' && req.headers.hasOwnProperty('access-control-request-headers')) {
        log('[policyParser Middleware] OPTIONS',req.path)
        var hasAuthInAccessControl = !!~req.headers['access-control-request-headers']
                                      .split(',').map(function (header) {
                                        return header.trim();
                                      }).indexOf('authorization');

        log('[policyParser Middleware] OPTIONS - hasAuthInAccessControl ',hasAuthInAccessControl)
        //if (hasAuthInAccessControl) {          
          return next();
        //}
      }

      let cleanPath = (req.path.indexOf('/v1') != -1 ) ? req.path.split('/v1')[1] : req.path
      let action = pathutil.asRpc(req.method, cleanPath)
      log("[policyParser Middleware] Action requested:", action, req.method, req.path, cleanPath)

      res.locals.tenantId = options.tenantMap[req.get('host')];
      if (Reflect.has(req.headers, 'x-tenant') && options.namespace === 'local') {
        res.locals.tenantId = req.headers['x-tenant'];
      }
      log('[policyParser Middleware] tenant requested:', res.locals.tenantId, 'Host', req.get('host'));

      //Public request
      if(!req.headers["authorization"] && (publicActions[action] || publicActions[req.method+cleanPath])){
        next()
      }else if(req.headers["x-authorization"]){
        log("x-authorization flow")
        let service = Service.fromJwtToken(tokenHeadersParser(req.headers), options.secret, options.jwtoptions)
        let policies = service.getPolicies().map(p => Policy.fromJson(p))
        let accessManager = new AccessManager(policies, options.service, action)
        
        res.locals['accessManager'] = accessManager
        if(service){
          res.locals['service'] = service;
          res.locals['internalRequest'] = {iss: service.iss, acd: service.aud};
        }
        next()
      }else{
        log("authorization flow")
        let identity =  Identity.fromJwtToken(tokenHeadersParser(req.headers), options.secret, options.jwtoptions)
        let policies = identity.getPolicies().map(p => Policy.fromJson(p))
        let accessManager = new AccessManager(policies, options.service, action)
        
        let actionAllowed = accessValidation(action, accessManager, req.method+cleanPath)
        log("[policyParser Middleware] Action allowed:", actionAllowed)
        
        if(!res.locals){
          res.locals = {identity:null,accessManager:null};
        } 
        res.locals['identity'] = identity
        res.locals['accessManager'] = accessManager

        log("[policyParser Middleware] res.locals ", res.locals)
        next()
      }
    }catch (e) {
      if (e instanceof UnauthorizedError) {
        log("UnauthorizedError Error",e.status);
        return res.status(e.status).send(e)
      }else if (e instanceof ForbiddenError) {
        log("ForbiddenError Error",e.status);
        return res.status(e.status).send(e)
      } else if (e instanceof TypeError) {
        // statements to handle TypeError exceptions
        return res.status(e.status || 500).send(e || {message:"Server Error"})
      } else if (e instanceof RangeError) {
        // statements to handle RangeError exceptions
        return res.status(e.status || 500).send(e || {message:"Server Error"})
      } else if (e instanceof EvalError) {
        // statements to handle EvalError exceptions
        return res.status(e.status || 500).send(e || {message:"Server Error"})
      } else {
        // statements to handle any unspecified exceptions
        //log(e); // pass exception object to error handler
        return res.status(e.status || 500).send(e || {message:"Server Error"})
      }       
    }    
  }
}

module.exports = expressMiddleware
