# Change Log
Cobuild Express Policy Middleware implementation follows [Semantic Versioning](http://semver.org/).

## [1.4.6] 2019-12-12
### Added
- update cobuild access manager v0.4.0 --> v0.4.1

## [1.4.5] 2019-09-30
### Added
- update cobuild access manager v0.3.2 --> v0.4.0

## [1.4.4] 2019-09-09
### Added
- new locals property tenantId

## [1.4.3] 2019-08-26
### Added
- update cobuild access manager v0.3.1 --> v0.3.2

## [1.4.2] 2019-08-23
### Added
- bug fixed typo

## [1.4.1] 2019-05-08
### Changed
- update cobuild access manager v0.3.0 --> v0.3.1

## [1.4.0] 2019-03-20
### Changed
- update cobuild access manager v0.2.2 --> v0.3.0

## [1.3.0] 2019-01-28
### Added
- Load Access Manager at public endpoints if valid authorization token sent


## [1.2.1] 2018-12-13
### Added
- package-lock.json for npm audit
- add eslintrc

### Changed
- Update token expiration unit test, exception validation
- cobuild-access-manager updated from v0.2.1 to v0.2.2
- debug updated from 3.0.1 to 4.1.0
- jsonwebtoken updated from 7.4.3 to 8.4.0 
- express updated from 4.15.4 to 4.16.4
- lodash updated from 4.17.4 4.17.11 
- joi dependency removed



## [1.2.0] 2017-07-27
### Added
- New flow to validate internal microservices communication with x-authorization header
- Usage of Kubernetes Secrets to support RS JWT signatures.
### Changed
- Use Cobuild-Access-Manager v0.2.1

## [1.1.0] 2017-01-03
### Added
- Use Cobuild-Access-Manager v0.2.0
- New error management. Send object instead of string
- ForbiddenError prototype

## [1.0.2] 2017-12-04
### Added
- Use Cobuild-Access-Manager v0.1.4

## [1.0.1] 2017-10-05
### Added
- Added joi dependency.
- Use Cobuild-Access-Manager v0.1.3

## [0.0.1] 2017-09-06
### Added
- Initial version of express middleware using the Access-Manager v0.1.0 
- Allow override of action access validation - accessManager.serviceActionAllowed() used by default
- Add in res.locals Identity and accessManager Objects 

