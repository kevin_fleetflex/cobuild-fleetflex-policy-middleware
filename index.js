const log               = require('debug')('cobuild-express-policy-middleware')
const expressMiddleware = require('./lib/expressMiddleware')

 /**
  * Exponse internal modules
  */
module.exports.expressMiddleware = expressMiddleware